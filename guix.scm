(use-modules (guix)
	     (guix build-system copy)
	     (guix git-download)
	     (guix licenses)
             (gnu packages scheme))

(define r7rs-path "lib/scheme/r7rs")
(define r7rs-http-1-1
  (let ((commit "023ae37013218de37e781ef109f9c151fe41163c")
	(revision "0.1"))
    (package
      (name "r7rs-http-1.1")
      (version (git-version "0.0.0" revision commit))
      (source
       (origin
	 (method git-fetch)
	 (uri (git-reference
	       (url "https://github.com/wanderer/http-1.1.git")
	       (commit commit)))
	 (sha256
	  (base32  "0f6mkyqpzlwdsr4jhpb31jrpz6j9bljm6ii78bsi6mvkpn4wmgii"))
	 (file-name (git-file-name name version))))
      (build-system copy-build-system)
      (arguments
       `(#:install-plan
	 '(("http.scm" ,(string-append r7rs-path "/arew/network/")))))
      (home-page "https://github.com/pre-srfi/http-1.1")
      (synopsis "SRFI libraries for Chez Scheme")
      (description
       "This package provides a collection of SRFI libraries for Chez Scheme.")
      (license expat))))

(define r7rs-srfi-209
  (let ((commit "290b01c99c3cd0a1576ab07221a0554deee48b00")
	(revision "0.1"))
    (package
      (name "r7rs-srfi-209")
      (version (git-version "0.0.0" revision commit))
      (source
       (origin
	 (method git-fetch)
	 (uri (git-reference
	       (url "https://github.com/scheme-requests-for-implementation/srfi-209.git")
	       (commit commit)))
	 (sha256
	  (base32 "1dlqypm08gp8kx7lb6m3lvy16wdqgg6klx4qap2v7jz29rc02jwc"))
	 (file-name (git-file-name name version))))
      (build-system copy-build-system)
      (arguments
       `(#:install-plan
	 '(("srfi" ,(string-append r7rs-path "/srfi")))))
      (home-page "https://github.com/scheme-requests-for-implementation/srfi-209")
      (synopsis "SRFI libraries for Chez Scheme")
      (description
       "This package provides a collection of SRFI libraries for Chez Scheme.")
      (license expat))))

(define r7rs-dictionaries
  (let ((commit "4ef561e0148b5c136e5bb1ab48d03a5200f50608")
	(revision "0.1"))
      (package
	(name "r7rs-dictionaries")
	(version (git-version "0.0.0" revision commit))
	(source
	 (origin
	   (method git-fetch)
	   (uri (git-reference
		 (url "https://github.com/pre-srfi/dictionaries.git")
		 (commit commit)))
	   (sha256
	    (base32 "0xqk4kjsvlxsv0mhkkm0q2rnqcjs0wac8bfrg89fs0s36psn9134"))
	   (file-name (git-file-name name version))))
	(build-system copy-build-system)
	(arguments
	 `(#:install-plan
	   '(("." ,(string-append "/" r7rs-path) #:include-regexp ("\\.scm$")))))
	(home-page "https://github.com/fedeinthemix/chez-srfi")
	(synopsis "SRFI libraries for Chez Scheme")
	(description
	 "This package provides a collection of SRFI libraries for Chez Scheme.")
	(license expat))))

(define wrapped-gauche
  (package
    (inherit gauche)
    (name "wrapped-gauche")
    (native-search-paths
     (list (search-path-specification
	    (variable "GAUCHE_LOAD_PATH")
	    (files `(,r7rs-path)))))))

(define httpRequest
  (let ((commit "565134bc5c0e54007861e3de3b7aff41c83b04a4")
	(revision "0.1"))
    (package
      (name "httpRequest")
      (version (git-version "0.0.0" revision commit))
      (source
       (origin
	 (method git-fetch)
	 (uri (git-reference
	       (url "https://gitlab.com/mjbecze/scheme-httpRequest.git")
	       (commit commit)))
	 (sha256
	  (base32 "0rp4asndfy5b75kpfamqfia4mcyp3hpdv640s939nxlmwci1kxrq"))
	 (file-name (git-file-name name version))))
      (build-system copy-build-system)
      (arguments
       `(#:install-plan
	 '(("." ,(string-append "/" r7rs-path) #:include-regexp ("\\.sld$")))))
      (propagated-inputs
       `(("r7rs-http-1-1" ,r7rs-http-1-1)
	 ("r7rs-srfi-209" ,r7rs-srfi-209)
	 ("r7rs-dictionaries" ,r7rs-dictionaries)))
      (home-page "https://gitlab.com/mjbecze/scheme-httpRequest")
      (synopsis "Make HTTP requests")
      (description
       "This package is for making simple HTTP requests in r7rs schemes.")
      (license gpl3+))))

httpRequest
