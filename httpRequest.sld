(define-library (httpRequest)
  (export
   http-request
   http-error?
   http-connect-error?
   http-timeout-error?
   http-response-error?
   http-redirect-error?
   http-error-payload)

  (import
   (scheme base)
   (scheme fixnum)
   (srfi 106)				; sockets
   (srfi 209) 				; enums
   (prefix (arew network http) arew:)
   (dictionaries))

  (begin
    (define error-types
      (make-enum-type '(connect timeout response redirect)))

    (define error-comparator (make-enum-comparator error-types))

    (define-record-type <http-error>
      (make-http-error type payload)
      http-error?
      (type http-error-type)
      (payload http-error-payload))

    (define (http-connect-error? obj)
      (=? error-comparator
	  (http-error-type obj) (enum-name->enum error-types 'connection)))

    (define (http-timeout-error? obj)
      (=? error-comparator
	  (http-error-type obj) (enum-name->enum error-types 'timeout)))

    (define (http-response-error? obj)
      (=? error-comparator
	  (http-error-type obj) (enum-name->enum error-types 'response)))

    (define (http-redirect-error? obj)
      (=? error-comparator
	  (http-error-type obj) (enum-name->enum error-types 'redirect)))

    (define (http-request request)
      (define (socket-generator socket)
	(lambda ()
	  (let ((data (socket-recv socket 1)))
	    (if (= 1 (bytevector-length data))
		(bytevector-u8-ref data 0)
		(eof-object)))))

      (define (socket-accumulator socket)
	;; There is no buffering.
	(lambda (data)
	  (if (fixnum? data)
	      (socket-send socket (bytevector data))
	      (let loop ((index 0))
		(unless (fx=? index (bytevector-length data))
		  (let ((sent (socket-send socket data)))
		    (loop (fx+ index sent))))))))

      (define (split-char-list ls c)
	(let loop ((rest ls)
		   (first '()))
	  (cond
	   ((null? rest) (values '() ls))
	   (else
	    (let ((char (car rest)))
	      (if (char=? char c)
		  (values first (cdr rest))
		  (loop (cdr rest)
			(append first
				(list char)))))))))

      (define (host-read ls)
	(split-char-list ls #\:))

      (define (parse-url url)
	(let*-values
	    (((url-ls) (string->list url))
	     ((host-port path) (split-char-list url-ls #\/))
	     ((host port) (split-char-list host-port #\:)))

	  (if (null? host)
	      (raise (make-http-error
		      (enum-name->enum error-types 'response)
		      "invalid url: no host"
		      url)))

	  (values (list->string host)
		  (list->string (cons #\/ path))
		  (list->string port))))

      (define (build-header host headers body)
	(dict->alist
	 (dict-adjoin!
	  headers
	  "accept" "*/*"
	  "user-agent" "scheme-httprequest/r6rs"
	  "host" host
	  "content-length" (number->string
			    (bytevector-length
			     (string->utf8 body))))))

      (let*-values (((version) '(1 . 1))
		    ((body) (dict-ref/default request 'body ""))
		    ((host path port) (parse-url (dict-ref request 'url)))
		    ((headers) (build-header
				host
				(dict-ref/default request 'headers '())
				body))
		    ((socket) (with-exception-handler
				  (lambda (x)
				    (raise (make-http-error
					    (enum-name->enum error-types 'connection)
					    x)))
				(lambda ()
				  (make-client-socket host port))))
		    ((acc) (socket-accumulator socket)))

	(arew:http-request-write
	 acc
	 (symbol->string (dict-ref request 'verb))
	 path
	 version
	 headers
	 body)

	(call-with-values
	    (lambda ()
	      (with-exception-handler
		  (lambda (x)
		    (when (arew:http-error? x)
		      (raise (make-http-error
			      (enum-name->enum error-types 'response)
			      (arew:http-error-message x)
			      (arew:http-error-payload x)))))
		(lambda ()
		  (arew:http-response-read
		   (socket-generator socket)))))

	  (lambda (version code reason headers body)
	    `((version . ,version)
	      (code . ,code)
	      (reason . ,reason)
	      (headers . ,headers)
	      (body . ,body))))))))
