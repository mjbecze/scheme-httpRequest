(define-library (httpRequest formData)
  (export
   formData
   formData-headers
   formData-body)
  (import
   (srfi 27) 				; randomish
   (scheme base)
   (scheme charset)
   (dictionaries))
  (begin
    (define CRLF "\r\n")
    (define SPC " ")
    (define (make-request-header name . value)
      (apply string-append `(,name ":" ,SPC ,@value ,CRLF)))

    (define (generate-boundary n)
      (let* ((chars (char-set->list
		     (char-set-intersection char-set:ascii char-set:letter+digit)))
	     (chars-len (length chars)))
	(let loop ((n n)
		   (boundary '()))
	  (if (= n 0)
	      (apply string boundary)
	      (loop (- n 1)
		    `(,@boundary
		      ,(list-ref chars (random-integer chars-len))))))))

    (define (formData . parts)
      `((boundary . ,(generate-boundary 20))
	(parts . ,parts)))

    (define (formData-headers form)
      (let ((boundary (dict-ref form 'boundary)))
	`(("content-type" . ,(string-append "multipart/form-data; boundary=" boundary)))))

    (define (formData-body form)
      (let ((parts (dict-ref form 'parts))
	    (boundary (dict-ref form 'boundary)))
	(let loop ((parts parts)
		   (string ""))
	  (if (null? parts)
	      (string-append string "--" boundary "--" CRLF)
	      (let* ((part (car parts))
		     (name (dict-ref part 'name))
		     (filename (dict-ref part 'filename)))
		(loop (cdr parts)
		      (string-append
		       string "--" boundary CRLF
		       (make-request-header "Content-Disposition"
					    (string-append "form-data; name=\"" name "\"; filename=\"" filename "\""))
		       (make-request-header "Content-Type" "application/octet-stream")
		       CRLF (dict-ref part 'content) CRLF)))))))))
