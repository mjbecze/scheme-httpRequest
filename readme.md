# httpRequest

Loosely based off of [johnwcowan/HttpRequest](https://github.com/johnwcowan/r7rs-work/blob/master/HttpRequest.md).

Key differences are that this implementation uses srfi-106 for sockets instead of using an accumulator.

# Example

```
(import
    (scheme base)
	(scheme write)
	(httpRequest)
	(httpRequest formData))

(define form
  (formData
   '((content . "안녕하세요 이것은 테스트입니다")
     (name . "file")
     (filename . "test.scm")
     (headers . ((content-type . "application/octet-stream"))))))

(define request
  `((url . "127.0.0.1:5001/api/v0/add")
    (verb . POST)
    (headers . ,(formData-headers form))
    (body . ,(formData-body form))))

(display (http-request request))
```

# Develop

To install the dependencies for this libary use `guix environment -l ./guix.scm`. This will also install Gauche Scheme.

# Liscense

GNU General Public License v3.0
